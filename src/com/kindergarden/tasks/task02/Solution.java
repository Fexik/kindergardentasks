package com.kindergarden.tasks.task02;

public class Solution {
    public static void main(String[] args) {

        String string = "Мова програмування Java зародилася в 1991 р. в лабораторіях компанії Sun Microsystems";

        System.out.println(string);
        System.out.println(reverseWord(string));
    }

    public static String reverseWord(String string){

        if (string == null) return "";
        StringBuilder stringBuilder = new StringBuilder();
        String[] words = string.split("\\s+");

        for (int i = words.length-1; i >= 0; i--)
            stringBuilder.append(words[i]).append(" ");

        return stringBuilder.toString().trim();
    }
}
