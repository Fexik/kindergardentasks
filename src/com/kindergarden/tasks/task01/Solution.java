package com.kindergarden.tasks.task01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {

    private static int[] numbers;

    private static List<Integer> maxList = new ArrayList<>();
    private static List<Integer> currentList = new ArrayList<>();
    private static int start;
    private static int end;

    public static void main(String[] args) {

        int i = 0;
        numbers = fillArrayRandomNumbers(30);
        searchSequenceNumbers(i);
        printSequence();


    }

    private static void searchSequenceNumbers(int k){
        int length = numbers.length;
        int startIndex = k;
        int endIndex = 0;
        int count = 0;
        int i = k;

        if (k == length) return;

        for (;i <= length-1; i++) {
            if(i == length-1 && !currentList.contains(numbers[i])) {
                currentList.add(numbers[i]);
                count++;
                endIndex = startIndex + count-1;
                break;
            }
            if (numbers[i] != numbers[i+1] && !currentList.contains(numbers[i+1])){
                currentList.add(numbers[i]);
                count++;
            } else {
                count++;
                endIndex = startIndex + count-1;
                currentList.add(numbers[endIndex]);
                break;
            }
        }

        if (currentList.size() >= maxList.size()) {
            maxList = new ArrayList<>(currentList);
            start = startIndex;
            end = endIndex;
        }
            currentList.clear();

        if (++k < length) searchSequenceNumbers(k);
    }


    public static int[] fillArrayRandomNumbers(int lengthArray){
        int[] array = new int[lengthArray];
        for (int i = 0; i < array.length; i++)
            array[i] = (int) Math.round((Math.random() * 50));
        return array;
    }

    private static void printSequence(){
        System.out.println(Arrays.toString(numbers));
        System.out.println();
        System.out.println("Sequence = " + Arrays.toString(maxList.toArray()));
        System.out.println("count: "+ maxList.size() + "; startIndex: " + start + "; " +"endIndex: " + end );
    }
}
