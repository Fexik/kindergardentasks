package com.kindergarden.tasks.task13;

/**
 * Created by Fexik on 16.05.2016.
 */
public class Solution {

    public static void main(String[] args) {

        System.out.println(Rectangle.searchSquaresCount(6,19) + " squares");

    }

    static class Rectangle {
        private static int countSquares;

        public static int searchSquaresCount(int height, int width){

            if (height == 0 || width == 0 ) return ++countSquares;

            if (height < width) {
                int newHeight = width - height;
                searchSquaresCount(newHeight, height);
            } else {
                int newWidth = height - width;
                searchSquaresCount(newWidth, width);
            }

            return countSquares++;
        }

    }

}
