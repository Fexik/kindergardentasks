package com.kindergarden.tasks.task03;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Solution {
    public static void main(String[] args) {
        List<Integer> StackOne = new ArrayList<>();
        List<Integer> StackTwo = new ArrayList<>();

        int sumStackOnePages = 0;
        int sumStackTwoPages = 0;
        int sumPages = 0;

        Integer[] books = fillArrayRandomNumbers(10);

        Arrays.sort(books, Collections.reverseOrder());

        for (Integer book : books) sumPages += book;

        int halfHeight = sumPages >>> 1;

        for (int i = 0; i < books.length; i++) {
              if ((books[i] + sumStackOnePages) < halfHeight){
                  sumStackOnePages += books[i];
                  StackOne.add(books[i]);
              } else {
                  sumStackTwoPages += books[i];
                  StackTwo.add(books[i]);
              }
        }

        System.out.println("Source:   " + Arrays.toString(books));
        System.out.println("StackOne: " + Arrays.toString(StackOne.toArray()) + " => " + sumStackOnePages);
        System.out.println("StackTwo: " + Arrays.toString(StackTwo.toArray()) + " => " + sumStackTwoPages);
    }


    public static Integer[] fillArrayRandomNumbers(int lengthArray){
        Integer[] array = new Integer[lengthArray];
        for (int i = 0; i < array.length; i++)
            array[i] = (int) Math.round((Math.random() * 500));
        return array;
    }
}
