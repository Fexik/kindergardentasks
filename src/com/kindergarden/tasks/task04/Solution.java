package com.kindergarden.tasks.task04;


public class Solution {
    private static int step;
    public static void main(String[] args) {

        HanoyTown(7, 'A', 'C', 'B');
        //(2^n)-1
        System.out.println("Steps = "+step);
    }

    public static void HanoyTown(int nLevel, char from, char to, char mid){
        if (nLevel > 0) {
            step++;
            HanoyTown(nLevel-1, from, mid, to);
            System.out.printf("%c ==> %c\n",from, to);
            HanoyTown(nLevel-1, mid, to, from);
        }
    }

}
