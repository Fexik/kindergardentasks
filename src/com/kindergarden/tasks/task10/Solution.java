package com.kindergarden.tasks.task10;

/**
 * Created by Fexik on 18.05.2016.
 */
public class Solution {

    public static void main(String[] args) {
        int[][] triangle = {
                {7},
                {8, 3},
                {8, 1, 0},
                {2, 7, 4, 5},
                {2, 4, 5, 5, 9},
                {2, 9, 7, 1, 5, 9},
                {2, 9, 7, 8, 9, 5, 9},
        };
        showMaxSum(triangle);

    }


    private static void showMaxSum(int[][] triangle) {
        int sum = triangle[0][0];
        int position = 0;
        int maxNumber = 0;

        for (int i = 1; i < triangle.length; i++) {

            int j = position;

            for (; j < triangle[i].length - 1; j++) {

                if (triangle[i][j] > triangle[i][j + 1]) {
                    maxNumber = triangle[i][j];
                    position = j;
                } else {
                    maxNumber = triangle[i][j + 1];
                    position = j + 1;
                }
                sum += maxNumber;
                break;
            }
        }
        System.out.println("Max sum: " + sum);
    }

}
