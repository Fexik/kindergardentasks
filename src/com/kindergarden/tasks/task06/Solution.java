package com.kindergarden.tasks.task06;

/**
 * Created by Fexik on 16.05.2016.
 */
public class Solution {
    public static void main(String args[])
    {
        int n = 4;
        int[][] a = new int[n][n];

        if (n % 4 > 0) System.err.print("Error: N is not a multiple of 4.");
        else {
            System.out.println("Source array: ");
            GenerateArray(a);
            PrintArray(a);
            System.out.println("Magic square: ");
            ReverseArray1(a);
            ReverseArray2(a);
            PrintArray(a);
        }
    }

    public static void PrintArray(int a[][])
    {
        for (int i = 0; i < a.length; i++)
        {
            for (int j = 0; j < a.length; j++)
            {
                System.out.print(a[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public static void GenerateArray(int a[][])
    {
        int k = 1;
        for (int i = 0; i < a.length; i++)
        {
            for (int j = 0; j < a.length; j++)
            {
                a[i][j] = k;
                k++;
            }
        }
    }

    public static void ReverseArray1(int a[][])
    {
        int t;
        for (int i = 0; i < a.length / 2; i++)
        {
            for (int j = 0; j < a.length / 2; j++)
            {
                if (i == j)
                {
                    t = a[i][j];
                    a[i][j] = a[a.length - 1 - i][a.length - 1 - j];
                    a[a.length - 1 - i][a.length - 1 - j] = t;
                }
            }
        }
    }

    public static void ReverseArray2(int a[][])
    {
        int t;
        for (int i = 0, j = a.length - 1 - i; i <= (a.length / 2) - 1; ++i, --j)
        {
            t = a[i][j];
            a[i][j] = a[j][i];
            a[j][i] = t;
        }
    }
}
