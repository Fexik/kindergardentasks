package com.kindergarden.tasks.task14;

/**
 * Created by Fexik on 14.05.2016.
 */
public class Solution {

    public static void main(String[] args) {

        int number = 100000;  //447
        System.out.println(number + " term of the sequence = " + findSequence(number));

        for (int i = 0; i < 5; i++) {
            int number2 = (int) (Math.random()*100000);
            System.out.println(number2 + " term of the sequence = " + findSequence(number2));
        }

    }

    private static int findSequence(int number){
        int i = 0;
        int count = 0;
        while (i <= number) i += ++count;
        return count;
    }
}
