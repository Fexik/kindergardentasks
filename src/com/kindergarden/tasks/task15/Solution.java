package com.kindergarden.tasks.task15;

/**
 * Created by Fexik on 17.05.2016.
 */
public class Solution {
    public static void main(String[] args) {

        for (int number = 1; number <= 50; number++)
            System.out.println("x[" + number + "] = " + getMember(number));

    }

    private static int getMember(int number) {
        return (number == 1) ? 1 : 1 - (getMember(number - degree(number)));
    }

    private static int degree(int number) {
        int max = 1;
        while (max*2 < number) max = max * 2;
        return max;
    }

}
